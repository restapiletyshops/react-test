import React from 'react';

var my_news = [
    {
        author: 'Саша Печкин',
        text: 'В четчерг, четвертого числа...',
        img:'http://st.kp.yandex.net/images/news/sm_2750579_1459411009.jpg'
    },
    {
        author: 'Просто Вася',
        text: 'Считаю, что $ должен стоить 35 рублей!',
        img:'http://st.kp.yandex.net/images/interview/sm_2750466_1459425348.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    }
];


var App = React.createClass({
    render: function(){
        return(
            <div className="app">
                Work!
                <News data={my_news} />
                <Comments />
            </div>
        );
    }
});

var News = React.createClass({
    render:function() {
        var data = this.props.data;
        var newsTemplate = data.map(function(item, index) {
            return (
                <div className="newitem" key={index}>
                    <p className="author">{item.author}</p>
                    <p className="text">{item.text}</p>
                    <div className="newimg">
                        <img src={item.img} />
                    </div>
                </div>
            );
        });

        return (
            <div className="news">
                {newsTemplate}
            </div>
        );
    }
});

var Comments = React.createClass({
    render:function() {
        return (
            <div className="comments">
                comments...
            </div>
        );
    }
});


ReactDOM.render(
    <App />,
    document.getElementById('root')
);