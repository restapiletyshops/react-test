import React from 'react';
import ReactDOM from 'react-dom';
import App from './classes/App.js';
import { Router, Route, Link, hashHistory } from 'react-router';

//let cp = new Point(23, 45);

//console.log(cp.toString());

console.log(App);

ReactDOM.render((
    <Router history={hashHistory}>
        <Route path="/" component={App.App} />
        <Route path="/user" component={App.User} />
    </Router>
), document.getElementById('root'))