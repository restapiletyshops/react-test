import React from 'react';

var my_news = [
    {
        author: 'Саша Печкин',
        text: 'В четчерг, четвертого числа...',
        img:'http://st.kp.yandex.net/images/news/sm_2750579_1459411009.jpg'
    },
    {
        author: 'Просто Вася',
        text: 'Считаю, что $ должен стоить 35 рублей!',
        img:'http://st.kp.yandex.net/images/interview/sm_2750466_1459425348.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    },
    {
        author: 'Гость',
        text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        img:'http://st.kp.yandex.net/images/news/sm_2750565_1459408760.jpg'
    }
];


class App extends React.Component {

    render () {
        return (
                <div className="main">
                    <h1>{this.props.type}</h1>
                    <Article data={my_news} />
                </div>
            );
    };
}

class Article extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        var data = this.props.data;
        var articles = data.map(function(item, index) {
            return (
                <div key={index} className="item">
                    <p>{item.author}</p>
                    <p>{item.text}</p>
                </div>
            );
        });
        return <div>{articles}</div>;
    };
}

class User extends React.Component {
    render () {
        return (
            <h3>Users</h3>
        );
    };
}

export default {App, User};